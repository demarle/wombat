
#include "wombat.h"
#include <iostream>

int main(int argc, char *argv[]) {

  std::vector<Level> levels;
  std::vector<Box> inBoxes;

  std::cout << "TEST 1 one level, one box of one cell" << std::endl;
  Level l;
  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 1;
  l.dims[1] = 1;
  l.dims[2] = 1;
  l.spacing[0] = 1.0;
  l.spacing[1] = 1.0;
  l.spacing[2] = 1.0;
  l.refinement[0] = 2;
  l.refinement[1] = 2;
  l.refinement[2] = 2;
  levels.push_back(l);

  Box b;
  b.parent = -1;
  b.owningrank = 0;
  b.level = 0;
  b.origin[0] = 0;
  b.origin[1] = 0;
  b.origin[2] = 0;
  b.dims[0] = 1;
  b.dims[1] = 1;
  b.dims[2] = 1;
 
  inBoxes.push_back(b);

  std::vector<Box> convexBoxes;
  convexBoxes = convexify(levels, inBoxes);

  std::cout << "TEST 1 results are" << std::endl;
  printBoxes(convexBoxes);
  std::cout << std::endl;

  std::cout << "TEST 2 one level, two boxes" << std::endl;
  levels.clear();
  inBoxes.clear();
  convexBoxes.clear();

  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 3;
  l.dims[1] = 3;
  l.dims[2] = 3;
  l.spacing[0] = 1.0;
  l.spacing[1] = 1.0;
  l.spacing[2] = 1.0;
  l.refinement[0] = 3;
  l.refinement[1] = 3;
  l.refinement[2] = 3;
  levels.push_back(l);

  b.parent = -1;
  b.owningrank = 0;
  b.level = 0;
  b.origin[0] = 0;
  b.origin[1] = 0;
  b.origin[2] = 0;
  b.dims[0] = 1;
  b.dims[1] = 1;
  b.dims[2] = 1;
  inBoxes.push_back(b);

  b.parent = -1;
  b.owningrank = 0;
  b.level = 0;
  b.origin[0] = 1;
  b.origin[1] = 1;
  b.origin[2] = 1;
  b.dims[0] = 2;
  b.dims[1] = 2;
  b.dims[2] = 2;
  inBoxes.push_back(b);

  convexBoxes = convexify(levels, inBoxes);
  std::cout << "TEST 2 results are" << std::endl;
  printBoxes(convexBoxes);
  std::cout << std::endl;

  std::cout << "TEST 3 two levels, two boxes" << std::endl;
  levels.clear();
  inBoxes.clear();
  convexBoxes.clear();

  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 3;
  l.dims[1] = 3;
  l.dims[2] = 3;
  l.spacing[0] = 1.0;
  l.spacing[1] = 1.0;
  l.spacing[2] = 1.0;
  l.refinement[0] = 3;
  l.refinement[1] = 3;
  l.refinement[2] = 3;
  levels.push_back(l);

  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 9;
  l.dims[1] = 9;
  l.dims[2] = 9;
  l.spacing[0] = 1. / 3.;
  l.spacing[1] = 1. / 3.;
  l.spacing[2] = 1. / 3.;
  l.refinement[0] = 3;
  l.refinement[1] = 3;
  l.refinement[2] = 3;
  levels.push_back(l);

  b.parent = -1;
  b.owningrank = 0;
  b.level = 0;
  b.origin[0] = 0;
  b.origin[1] = 0;
  b.origin[2] = 0;
  b.dims[0] = 3;
  b.dims[1] = 3;
  b.dims[2] = 3;
  inBoxes.push_back(b);

  b.parent = 0;
  b.owningrank = 0;
  b.level = 1;
  b.origin[0] = 3;
  b.origin[1] = 0;
  b.origin[2] = 0;
  b.dims[0] = 6;
  b.dims[1] = 3;
  b.dims[2] = 3;
  inBoxes.push_back(b);

  convexBoxes = convexify(levels, inBoxes);
  std::cout << "TEST 3 results are" << std::endl;
  printBoxes(convexBoxes);
  std::cout << std::endl;

  std::cout << "TEST 4 two levels, one L0 box, two L1 boxes" << std::endl;
  levels.clear();
  inBoxes.clear();
  convexBoxes.clear();

  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 3;
  l.dims[1] = 3;
  l.dims[2] = 3;
  l.spacing[0] = 1.0;
  l.spacing[1] = 1.0;
  l.spacing[2] = 1.0;
  l.refinement[0] = 3;
  l.refinement[1] = 3;
  l.refinement[2] = 3;
  levels.push_back(l);

  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 9;
  l.dims[1] = 9;
  l.dims[2] = 9;
  l.spacing[0] = 1. / 3.;
  l.spacing[1] = 1. / 3.;
  l.spacing[2] = 1. / 3.;
  l.refinement[0] = 3;
  l.refinement[1] = 3;
  l.refinement[2] = 3;
  levels.push_back(l);

  b.parent = -1;
  b.owningrank = 0;
  b.level = 0;
  b.origin[0] = 0;
  b.origin[1] = 0;
  b.origin[2] = 0;
  b.dims[0] = 3;
  b.dims[1] = 3;
  b.dims[2] = 3;
  inBoxes.push_back(b);

  b.parent = 0;
  b.owningrank = 0;
  b.level = 1;
  b.origin[0] = 0;
  b.origin[1] = 0;
  b.origin[2] = 0;
  b.dims[0] = 3;
  b.dims[1] = 3;
  b.dims[2] = 3;
  inBoxes.push_back(b);

  b.parent = 0;
  b.owningrank = 0;
  b.level = 1;
  b.origin[0] = 3;
  b.origin[1] = 3;
  b.origin[2] = 3;
  b.dims[0] = 3;
  b.dims[1] = 6;
  b.dims[2] = 3;
  inBoxes.push_back(b);

  convexBoxes = convexify(levels, inBoxes);
  std::cout << "TEST 4 results are" << std::endl;
  printBoxes(convexBoxes);
  std::cout << std::endl;


  std::cout << "TEST 5 three levels, one L0 box, one L1 box, one L2 box" << std::endl;
  levels.clear();
  inBoxes.clear();
  convexBoxes.clear();

  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 3;
  l.dims[1] = 3;
  l.dims[2] = 3;
  l.spacing[0] = 1.0;
  l.spacing[1] = 1.0;
  l.spacing[2] = 1.0;
  l.refinement[0] = 3;
  l.refinement[1] = 3;
  l.refinement[2] = 3;
  levels.push_back(l);

  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 9;
  l.dims[1] = 9;
  l.dims[2] = 9;
  l.spacing[0] = 1. / 3.;
  l.spacing[1] = 1. / 3.;
  l.spacing[2] = 1. / 3.;
  l.refinement[0] = 3;
  l.refinement[1] = 3;
  l.refinement[2] = 3;
  levels.push_back(l);

  l.origin[0] = 0;
  l.origin[1] = 0;
  l.origin[2] = 0;
  l.dims[0] = 27;
  l.dims[1] = 27;
  l.dims[2] = 27;
  l.spacing[0] = 1. / 3. / 3.;
  l.spacing[1] = 1. / 3. / 3.;
  l.spacing[2] = 1. / 3. / 3.;
  l.refinement[0] = 3;
  l.refinement[1] = 3;
  l.refinement[2] = 3;
  levels.push_back(l);

  b.parent = -1;
  b.owningrank = 0;
  b.level = 0;
  b.origin[0] = 0;
  b.origin[1] = 0;
  b.origin[2] = 0;
  b.dims[0] = 3;
  b.dims[1] = 3;
  b.dims[2] = 3;
  inBoxes.push_back(b);

  b.parent = 0;
  b.owningrank = 0;
  b.level = 1;
  b.origin[0] = 0;
  b.origin[1] = 0;
  b.origin[2] = 0;
  b.dims[0] = 3;
  b.dims[1] = 3;
  b.dims[2] = 3;
  inBoxes.push_back(b);

  b.parent = 1;
  b.owningrank = 0;
  b.level = 2;
  b.origin[0] = 0;
  b.origin[1] = 3;
  b.origin[2] = 0;
  b.dims[0] = 9;
  b.dims[1] = 3;
  b.dims[2] = 3;
  inBoxes.push_back(b);

  convexBoxes = convexify(levels, inBoxes);
  std::cout << "TEST 5 results are" << std::endl;
  printBoxes(convexBoxes);
  std::cout << std::endl;

  return 0;
}

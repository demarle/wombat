// Copyright 2021 Intel Co.
// SPDX-License-Identifier: Apache-2.0

#pragma once
#include <vector>

typedef struct Level {
  double origin[3]; //worldspace lower left x,y,z coordinate
  double spacing[3]; //wordspace x,y,z size of one cell, this is required to be a multiple of its child level
  int dims[3]; //global number of cells at this level
  int refinement[3]; //how much finer children's spacing is, ex 2 if my spacing is 1.0,1.0,1.0 and childrens' spacing is .5,.5,.5
} Level;

typedef struct Box {
  int owningrank; //the mpi rank of the owner of this box
  int level; //the level that this box belongs to
  int parent; //index of parent at previous level within inBoxes array.
  int origin[3]; //index into level of lower left corner
  int dims[3]; //index space i,j,k number of cells
} Box;

//Takes the AMR mesh consisting of levels and inBoxes and populates
//convexBoxes, which is an equivaltent mesh consisting of only convex boxes
//with course cells removed from each level. Returns true on success and
//false on failure. Overwrites convexBoxes, does not modify levels or inBoxes.
std::vector<Box> convexify(std::vector<Level> levels, std::vector<Box> inBoxes);


void printBox(Box box);
void printBoxes(std::vector<Box> boxes);

#include "wombat.h"

#include <iostream>

std::vector<Level> levels;
std::vector<Box> inBoxes;
std::vector<Box> convexBoxes;

typedef std::pair<int, Box> BoxWithID;

void printBox(Box box) {
    std::cout << "Parent = " << box.parent << " level " << box.level << " owner " << box.owningrank << std::endl;
    std::cout << "origin = " << box.origin[0] << "," << box.origin[1] << "," << box.origin[2] << std::endl;
    std::cout << "dims = " << box.dims[0] << "," << box.dims[1] <<  "," << box.dims[2] << std::endl;
}

void printBoxes(std::vector<Box> boxes)
{
    std::cout << "numBoxes = " << boxes.size() << std::endl;
	for (int b = 0; b < static_cast<int>(boxes.size()); ++b) {
		printBox(boxes[b]);
	}
}

bool subdivide(Box box, std::vector<Box> children, int axis) {

  if (children.size() == 0) {
      // no children, therefore convex
      convexBoxes.push_back(box); 
	  // std::cout << "no children, convex " << std::endl;
	  return true;
  }

  if (box.dims[0] == 1 && box.dims[1] == 1 && box.dims[2] == 1) {
      // std::cout << "unit cell, convex but refined" << std::endl;
	  return true;
  }

  if (box.dims[axis] > 1) {
	  // split in two and recurse into both halves
	  // std::cout << "DIVIDE on AXIS " << axis << std::endl;
	  // std::cout << "input " << std::endl;
	  // printBox(box);
	  // std::cout << "------" << std::endl;

	  // todo: align split with child edge as an optimization to make fewer splits
	  Box lhalf = box;
	  lhalf.origin[axis] = box.origin[axis];
	  lhalf.dims[axis] = box.dims[axis] / 2;
	  // printBox(lhalf);
	  Box rhalf = box;
	  rhalf.origin[axis] = box.origin[axis] + (box.dims[axis] / 2);
	  rhalf.dims[axis] = box.dims[axis] - (box.dims[axis] / 2); // account for integer division
	  // printBox(rhalf);

	  int refinement = levels[box.level].refinement[axis];
	  int middle = (box.origin[axis] + (box.dims[axis] / 2)) * refinement;
	  // std::cout << "MIDLINE " << middle << std::endl;

	  std::vector<Box> leftChildren;
	  std::vector<Box> rightChildren;
	  // decide if children are children of left half, right half or both
	  for (int c = 0; c < static_cast<int>(children.size()); ++c) {
		  Box child = children[c];
		  // std::cout << "EXAMINE CHILD " << std::endl;
		  // printBox(child);

		  int cleftedge = child.origin[axis];
		  int crightedge = cleftedge + child.dims[axis];
		  // std::cout << "CLEFT, CRIGHT " << cleftedge << "," << crightedge << std::endl;
		  if (crightedge <= middle) {
			  leftChildren.push_back(child);
			  // std::cout << "IS LEFT" << std::endl;
		  }
		  else if (cleftedge >= middle) {
			  rightChildren.push_back(child);
			  // std::cout << "IS RIGHT" << std::endl;
		  }
		  else {
			  leftChildren.push_back(child);
			  rightChildren.push_back(child);
			  // std::cout << "IS BOTH" << std::endl;
		  }
	  }
	  // std::cout << leftChildren.size() << " left " << std::endl;
	  // std::cout << rightChildren.size() << " right " << std::endl;
	  subdivide(lhalf, leftChildren, (axis + 1) % 3);
	  subdivide(rhalf, rightChildren, (axis + 1) % 3);
  }
  else {
	  subdivide(box, children, (axis + 1) % 3);
  }
  return true;
}

std::vector<Box> convexify(std::vector<Level> _levels, std::vector<Box> _inBoxes) {
	levels = _levels;
	inBoxes = _inBoxes;
	convexBoxes = std::vector<Box>();

	for (int l = 0; l < static_cast<int>(levels.size()); ++l) {
		// std::cout << "Working on Level " << l << std::endl;
		std::vector<BoxWithID> boxesAtLevel;
		for (int b = 0; b < static_cast<int>(inBoxes.size()); ++b) {
			if (inBoxes[b].level == l) {
				boxesAtLevel.push_back(BoxWithID(b, inBoxes[b]));
			}
		}
		// std::cout << boxesAtLevel.size() << " boxes at level " << l << std::endl;
		for (int b = 0; b < static_cast<int>(boxesAtLevel.size()); ++b) {
			Box box = boxesAtLevel[b].second;
			int id = boxesAtLevel[b].first;
			std::vector<Box> children;
			for (int c = 0; c < static_cast<int>(inBoxes.size()); ++c) {
				if (inBoxes[c].parent == id) {
					children.push_back(inBoxes[c]);
				}
			}
			// std::cout << "box " << b << " has " << children.size() << " children" << std::endl;
			subdivide(box, children, 0);
		}
	}
	return convexBoxes;
}